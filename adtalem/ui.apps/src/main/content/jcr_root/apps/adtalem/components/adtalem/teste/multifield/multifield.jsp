<%@ page import="com.adobe.granite.ui.components.Config" %>
<%@include file="/libs/granite/ui/global.jsp" %>
<sling:include resourceType="/libs/granite/ui/components/foundation/form/multifield"/>
 
<%
    Config mCfg = cmp.getConfig();
 
    Resource mField = mCfg.getChild("field");
 
    ValueMap mVM = mField.getParent().adaptTo(ValueMap.class);
 
    String mFieldLimit = mVM.get("fieldLimit", "");
 
    if(mFieldLimit.equals("")){
        return;
    }
 
    String countValidatorId = "multifield-validator-" + System.currentTimeMillis(); //or some random number
%>
 
<input type=text style='display:none' id="<%=countValidatorId%>" value="<%=mFieldLimit%>"/>
 
<script>
    (function($){
        var fieldErrorEl = $("<span class='coral-Form-fielderror coral-Icon coral-Icon--alert coral-Icon--sizeS' " +
                                "data-init='quicktip' data-quicktip-type='error' />");
 
        var $countValidatorField = $("#<%=countValidatorId%>"),
            $multifield = $countValidatorField.prev().find(".coral-Multifield"),
            fieldLimit = $multifield.data("fieldlimit"),
            count = $(".cq-dialog-content").find(".coral-Multifield-input").length;
     
        //add validator on the multifield
        $.validator.register({
            selector: $countValidatorField,
            validate: validate,
            show: show,
            clear: clear
        });
 
        $multifield.on("click", ".js-coral-Multifield-add", function (e) {
        	count = $(".cq-dialog-content").find(".coral-Multifield-input").length;
            ++count;
            adjustMultifieldUI();
        });
 
        $multifield.on("click", ".js-coral-Multifield-remove", function (e) {
        	count = $(".cq-dialog-content").find(".coral-Multifield-input").length;
            --count;
            adjustMultifieldUI();
        });
        
        function adjustMultifieldUI(){
            var $addButton = $(".cq-dialog-content").find(".js-coral-Multifield-add");
            
            $addButton.each(function( index, value ) {
            	  if(fieldLimit != null && count >= fieldLimit){
                      $(value).attr('disabled','disabled');
                  }else{
                	  $(value).removeAttr('disabled');
                  }
            });
 		
        }
 
        function validate(){
            if(fieldLimit && count <= fieldLimit){
                return null;
            }
            return "O limite m�ximo de slides � " + fieldLimit;
        }
 
        function show($el, message){
            this.clear($countValidatorField);
 
            var arrow = $multifield.closest("form").hasClass("coral-Form--vertical") ? "right" : "top";
 
            fieldErrorEl.clone()
                    .attr("data-quicktip-arrow", arrow)
                    .attr("data-quicktip-content", message)
                    .insertAfter($multifield);
        }
 
        function clear(){
            $multifield.nextAll(".coral-Form-fielderror").tooltip("hide").remove();
        }
    })(jQuery);
</script>